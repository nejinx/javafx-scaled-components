package net.casualcoding.fxgui;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;

public class Main extends Application
{
	private BorderPane rootLayout;
	private Stage primaryStage;
	
	@Override
	public void start(Stage primaryStage)
	{
		this.primaryStage = primaryStage;
		
		try
		{
			rootLayout = new BorderPane();
			Scene scene = new Scene(rootLayout, 400, 400);
			scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
			
			primaryStage.addEventHandler(WindowEvent.WINDOW_HIDDEN, event ->
			{
				System.out.println("Window Hidden");
			});
			
			primaryStage.addEventHandler(WindowEvent.WINDOW_SHOWN, event ->
			{
				System.out.println("Window Shown");
			});
			primaryStage.setScene(scene);
			primaryStage.show();
			
			showCustomComponent();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
	
	private void showCustomComponent()
	{
		try
		{
			FXMLLoader loader = new FXMLLoader(getClass().getResource("Main.fxml"));
			AnchorPane root;
			root = (AnchorPane)loader.load();
			MainController controller = (MainController)loader.getController();
			controller.setMainApp(this);
			rootLayout.setCenter(root);
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	public Stage getPrimaryStage()
	{
		return primaryStage;
	}
	
	public static void main(String[] args)
	{
		launch(args);
	}
}
