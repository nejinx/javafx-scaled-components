package net.casualcoding.fxgui.listener;

import net.casualcoding.fxgui.util.Measure;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Region;

public class PrefWidthScaler implements ChangeListener<Number>
{
	private Region region;
	private String debugMessage;
	
	public PrefWidthScaler(Region region)
	{		
		this.region = region;
		region.prefWidthProperty().addListener(this);
	}
	
	public void setDebugMessage(String debugMessage)
	{
		this.debugMessage = debugMessage;
	}
	
	@Override
	public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
	{
		region.prefWidthProperty().removeListener(this);
		
		if (debugMessage != null)
		{
			System.out.println(debugMessage);
		}
		
		region.setPrefWidth(Measure.getEm(newValue.doubleValue()));
		region.setMaxWidth(Measure.getEm(newValue.doubleValue()));
	}
}
