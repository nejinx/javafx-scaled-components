package net.casualcoding.fxgui.listener;

import net.casualcoding.fxgui.util.Measure;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.scene.layout.Region;

public class PrefHeightScaler implements ChangeListener<Number>
{
	private Region region;
	private String debugMessage;
	
	public PrefHeightScaler(Region region)
	{		
		this.region = region;
		region.prefHeightProperty().addListener(this);
	}
	
	public void setDebugMessage(String debugMessage)
	{
		this.debugMessage = debugMessage;
	}
	
	@Override
	public void changed(ObservableValue<? extends Number> observable, Number oldValue, Number newValue)
	{
		region.prefHeightProperty().removeListener(this);
		
		if (debugMessage != null)
		{
			System.out.println(debugMessage);
		}
		
		region.setPrefHeight(Measure.getEm(newValue.doubleValue()));
		region.setMaxHeight(Measure.getEm(newValue.doubleValue()));
	}
}
