package net.casualcoding.fxgui.component;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.ComboBox;

public class ScaledComboBox<T> extends ComboBox<T>
{
	final ScaledComboBoxController controller;
	
	public ScaledComboBox()
	{
		controller = load();
	}
	
	private ScaledComboBoxController load()
	{
       final FXMLLoader loader = new FXMLLoader();
        
        // fx:root is this node.
        loader.setRoot(this);
        
        // The FXMLLoader should use the class loader that loaded
        // this class (ScaledButton).
        loader.setClassLoader(this.getClass().getClassLoader());
        
        // ScaledLabel.fxml contains the configuration for 'this'
        loader.setLocation(this.getClass().getResource("ScaledComboBox.fxml"));
        
        try
        {
            final Object root = loader.load();
            assert root == this;
        }
        catch (IOException ex)
        {
            throw new IllegalStateException(ex);
        }
        
        final ScaledComboBoxController scaledComboBoxController = loader.getController();
        assert scaledComboBoxController != null;
        return scaledComboBoxController;
	}		
}
