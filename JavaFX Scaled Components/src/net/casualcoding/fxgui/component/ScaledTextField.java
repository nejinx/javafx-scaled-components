package net.casualcoding.fxgui.component;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.control.TextField;

public class ScaledTextField extends TextField
{
    final ScaledTextFieldController controller;
    
    public ScaledTextField()
    {
        controller = load();
    }
    
    private ScaledTextFieldController load()
    {
        final FXMLLoader loader = new FXMLLoader();
        
        // fx:root is this node.
        loader.setRoot(this);
        
        // The FXMLLoader should use the class loader that loaded
        // this class (ScaledTextField).
        loader.setClassLoader(this.getClass().getClassLoader());
        
        // ScaledTextField.fxml contains the configuration for 'this'
        loader.setLocation(this.getClass().getResource("ScaledTextField.fxml"));
        
        try
        {
            final Object root = loader.load();
            assert root == this;
        }
        catch (IOException ex)
        {
            throw new IllegalStateException(ex);
        }
        
        final ScaledTextFieldController scaledTextFieldController = loader.getController();
        assert scaledTextFieldController != null;
        return scaledTextFieldController;
    }	
}
