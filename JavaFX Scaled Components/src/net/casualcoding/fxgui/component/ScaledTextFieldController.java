package net.casualcoding.fxgui.component;

import net.casualcoding.fxgui.listener.PrefHeightScaler;
import net.casualcoding.fxgui.listener.PrefWidthScaler;

import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;

// E:\Dropbox\programming\Java\javafx-workspace\Gui Building\src\application\gui\custom
public class ScaledTextFieldController
{	
	@FXML
	private TextField scaledTextField;

	@FXML
	private void initialize()
	{
		System.out.println("Loaded ScaledTextField");
		
		PrefWidthScaler pws = new PrefWidthScaler(scaledTextField);
		pws.setDebugMessage("### scaledTextField.prefWidthProperty() ###");
		
		PrefHeightScaler phs = new PrefHeightScaler(scaledTextField);
		phs.setDebugMessage("### scaledTextField.prefHeightProperty() ###");
		
		scaledTextField.addEventHandler(MouseEvent.MOUSE_ENTERED, event ->
		{
			System.out.println("Mouse Entered");
			System.out.println("prefWidth: " + scaledTextField.getPrefWidth());
		});
	}	
}
