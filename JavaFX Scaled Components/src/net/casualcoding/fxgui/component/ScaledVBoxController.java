package net.casualcoding.fxgui.component;

import javafx.fxml.FXML;
import javafx.scene.layout.VBox;
import net.casualcoding.fxgui.listener.PrefHeightScaler;
import net.casualcoding.fxgui.listener.PrefWidthScaler;

public class ScaledVBoxController
{
	@FXML
	private VBox scaledVBox;

	@FXML
	private void initialize()
	{	
		System.out.println("Loaded ScaledVBox");		

		PrefWidthScaler pws = new PrefWidthScaler(scaledVBox);
		pws.setDebugMessage("### scaledVBox.prefWidthProperty() ###");

		PrefHeightScaler phs = new PrefHeightScaler(scaledVBox);
		phs.setDebugMessage("### scaledVBox.prefHeightProperty() ###");
	}
}
