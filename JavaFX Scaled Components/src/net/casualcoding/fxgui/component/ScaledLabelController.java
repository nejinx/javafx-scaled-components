package net.casualcoding.fxgui.component;

import net.casualcoding.fxgui.listener.PrefHeightScaler;
import net.casualcoding.fxgui.listener.PrefWidthScaler;
import net.casualcoding.fxgui.util.Measure;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

public class ScaledLabelController
{
	@FXML
	private Label scaledLabel;

	@FXML
	private void initialize()
	{			
		System.out.println("Loaded ScaledLabel");

		PrefWidthScaler pws = new PrefWidthScaler(scaledLabel);
		pws.setDebugMessage("### scaledLabel.prefWidthProperty() ###");
		
		PrefHeightScaler phs = new PrefHeightScaler(scaledLabel);
		phs.setDebugMessage("### scaledLabel.prefHeightProperty() ###");
		
		// Resize Label font
		scaledLabel.fontProperty().addListener(new ChangeListener<Font>()
		{
			@Override
			public void changed(ObservableValue<? extends Font> observable, Font oldValue, Font newValue)
			{				
				scaledLabel.fontProperty().removeListener(this);				
				Font oldFont = scaledLabel.fontProperty().get();
				
				String output = String.format("Font\n\tFamily = %s\n\tName = %s\n\tSize = %s\n\tStyle = %s",
						oldFont.getFamily(), oldFont.getName(), oldFont.getSize(), oldFont.getStyle());
				System.out.println(output);
				
				System.out.println("Adjusted: " + Measure.getEm(oldFont.getSize()));
				System.out.println("Em: " + Measure.getEm());
				
				Font newFont = new Font(oldFont.getName(), Measure.getEm(oldFont.getSize()));
				scaledLabel.fontProperty().set(newFont);
				scaledLabel.fontProperty().addListener(this);
			}			
		});		
	}	
}
