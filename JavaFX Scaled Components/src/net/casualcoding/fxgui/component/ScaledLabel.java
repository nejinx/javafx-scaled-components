package net.casualcoding.fxgui.component;

import java.io.IOException;




import net.casualcoding.fxgui.util.Measure;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Label;
import javafx.scene.text.Font;

public class ScaledLabel extends Label
{
	final ScaledLabelController controller;
	
	public ScaledLabel()
	{
		final ScaledLabel self = this;
		
		controller = load();
	}
	
	private ScaledLabelController load()
	{
       final FXMLLoader loader = new FXMLLoader();
        
        // fx:root is this node.
        loader.setRoot(this);
        
        // The FXMLLoader should use the class loader that loaded
        // this class (ScaledLabel).
        loader.setClassLoader(this.getClass().getClassLoader());
        
        // ScaledLabel.fxml contains the configuration for 'this'
        loader.setLocation(this.getClass().getResource("ScaledLabel.fxml"));
        
        try
        {
            final Object root = loader.load();
            assert root == this;
        }
        catch (IOException ex)
        {
            throw new IllegalStateException(ex);
        }
        
        final ScaledLabelController scaledLabelController = loader.getController();
        assert scaledLabelController != null;
        return scaledLabelController;
	}	
}
