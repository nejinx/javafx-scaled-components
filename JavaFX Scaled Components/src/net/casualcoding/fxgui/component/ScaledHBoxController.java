package net.casualcoding.fxgui.component;

import javafx.fxml.FXML;
import javafx.scene.layout.HBox;
import net.casualcoding.fxgui.listener.PrefHeightScaler;
import net.casualcoding.fxgui.listener.PrefWidthScaler;

public class ScaledHBoxController
{
	@FXML
	private HBox scaledHBox;

	@FXML
	private void initialize()
	{	
		System.out.println("Loaded ScaledHBox");		

		PrefWidthScaler pws = new PrefWidthScaler(scaledHBox);
		pws.setDebugMessage("### scaledHBox.prefWidthProperty() ###");

		PrefHeightScaler phs = new PrefHeightScaler(scaledHBox);
		phs.setDebugMessage("### scaledHBox.prefHeightProperty() ###");
	}
}
