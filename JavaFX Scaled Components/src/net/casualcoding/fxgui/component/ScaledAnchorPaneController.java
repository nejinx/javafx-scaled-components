package net.casualcoding.fxgui.component;

import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;

import net.casualcoding.fxgui.listener.PrefHeightScaler;
import net.casualcoding.fxgui.listener.PrefWidthScaler;

public class ScaledAnchorPaneController
{
	@FXML
	private AnchorPane scaledAnchorPane;

	@FXML
	private void initialize()
	{	
		System.out.println("Loaded ScaledAnchorPane");		

		PrefWidthScaler pws = new PrefWidthScaler(scaledAnchorPane);
		pws.setDebugMessage("### scaledAnchorPane.prefWidthProperty() ###");

		PrefHeightScaler phs = new PrefHeightScaler(scaledAnchorPane);
		phs.setDebugMessage("### scaledAnchorPane.prefHeightProperty() ###");
	}
}
