package net.casualcoding.fxgui.component;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.AnchorPane;

public class ScaledAnchorPane extends AnchorPane
{
	final ScaledAnchorPaneController controller;
	
	public ScaledAnchorPane()
	{
		controller = load();
	}
	
	private ScaledAnchorPaneController load()
	{
       final FXMLLoader loader = new FXMLLoader();
        
        // fx:root is this node.
        loader.setRoot(this);
        
        // The FXMLLoader should use the class loader that loaded
        // this class (ScaledButton).
        loader.setClassLoader(this.getClass().getClassLoader());
        
        // ScaledLabel.fxml contains the configuration for 'this'
        loader.setLocation(this.getClass().getResource("ScaledAnchorPane.fxml"));
        
        try
        {
            final Object root = loader.load();
            assert root == this;
        }
        catch (IOException ex)
        {
            throw new IllegalStateException(ex);
        }
        
        final ScaledAnchorPaneController scaledAnchorPaneController = loader.getController();
        assert scaledAnchorPaneController != null;
        return scaledAnchorPaneController;
	}		
}
