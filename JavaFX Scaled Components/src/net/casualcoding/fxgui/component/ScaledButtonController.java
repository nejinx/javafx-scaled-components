package net.casualcoding.fxgui.component;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import net.casualcoding.fxgui.listener.PrefHeightScaler;
import net.casualcoding.fxgui.listener.PrefWidthScaler;

public class ScaledButtonController
{
	@FXML
	private Button scaledButton;

	@FXML
	private void initialize()
	{	
		System.out.println("Loaded ScaledButton");		

		PrefWidthScaler pws = new PrefWidthScaler(scaledButton);
		pws.setDebugMessage("### scaledButton.prefWidthProperty() ###");

		PrefHeightScaler phs = new PrefHeightScaler(scaledButton);
		phs.setDebugMessage("### scaledButton.prefHeightProperty() ###");
	}
}