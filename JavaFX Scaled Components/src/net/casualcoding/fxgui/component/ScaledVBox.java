package net.casualcoding.fxgui.component;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.VBox;

public class ScaledVBox extends VBox
{
	final ScaledVBoxController controller;
	
	public ScaledVBox()
	{
		controller = load();
	}
	
	private ScaledVBoxController load()
	{
       final FXMLLoader loader = new FXMLLoader();
        
        // fx:root is this node.
        loader.setRoot(this);
        
        // The FXMLLoader should use the class loader that loaded
        // this class (ScaledButton).
        loader.setClassLoader(this.getClass().getClassLoader());
        
        // ScaledLabel.fxml contains the configuration for 'this'
        loader.setLocation(this.getClass().getResource("ScaledVBox.fxml"));
        
        try
        {
            final Object root = loader.load();
            assert root == this;
        }
        catch (IOException ex)
        {
            throw new IllegalStateException(ex);
        }
        
        final ScaledVBoxController scaledVBoxController = loader.getController();
        assert scaledVBoxController != null;
        return scaledVBoxController;
	}		
}
