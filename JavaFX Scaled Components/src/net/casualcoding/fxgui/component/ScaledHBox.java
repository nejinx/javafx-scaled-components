package net.casualcoding.fxgui.component;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.HBox;

public class ScaledHBox extends HBox
{
	final ScaledHBoxController controller;
	
	public ScaledHBox()
	{
		controller = load();
	}
	
	private ScaledHBoxController load()
	{
       final FXMLLoader loader = new FXMLLoader();
        
        // fx:root is this node.
        loader.setRoot(this);
        
        // The FXMLLoader should use the class loader that loaded
        // this class (ScaledButton).
        loader.setClassLoader(this.getClass().getClassLoader());
        
        // ScaledLabel.fxml contains the configuration for 'this'
        loader.setLocation(this.getClass().getResource("ScaledHBox.fxml"));
        
        try
        {
            final Object root = loader.load();
            assert root == this;
        }
        catch (IOException ex)
        {
            throw new IllegalStateException(ex);
        }
        
        final ScaledHBoxController scaledHBoxController = loader.getController();
        assert scaledHBoxController != null;
        return scaledHBoxController;
	}		
}
