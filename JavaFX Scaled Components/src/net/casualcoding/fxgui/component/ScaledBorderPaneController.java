package net.casualcoding.fxgui.component;

import javafx.fxml.FXML;
import javafx.scene.layout.BorderPane;
import net.casualcoding.fxgui.listener.PrefHeightScaler;
import net.casualcoding.fxgui.listener.PrefWidthScaler;

public class ScaledBorderPaneController
{
	@FXML
	private BorderPane scaledBorderPane;

	@FXML
	private void initialize()
	{	
		System.out.println("Loaded ScaledBorderPane");		

		PrefWidthScaler pws = new PrefWidthScaler(scaledBorderPane);
		pws.setDebugMessage("### scaledBorderPane.prefWidthProperty() ###");

		PrefHeightScaler phs = new PrefHeightScaler(scaledBorderPane);
		phs.setDebugMessage("### scaledBorderPane.prefHeightProperty() ###");
	}
}
