package net.casualcoding.fxgui.component;

import net.casualcoding.fxgui.listener.PrefHeightScaler;
import net.casualcoding.fxgui.listener.PrefWidthScaler;

import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;

public class ScaledComboBoxController
{
	@FXML
	private ComboBox<?> scaledComboBox;
	
	@FXML
	private void initialize()
	{
		System.out.println("Loaded ScaledComboBox");
		
		PrefWidthScaler pws = new PrefWidthScaler(scaledComboBox);
		pws.setDebugMessage("### scaledComboBox.prefWidthProperty() ###");
		
		PrefHeightScaler phs = new PrefHeightScaler(scaledComboBox);
		phs.setDebugMessage("### scaledComboBox.prefHeightProperty() ###");
	}
}
