package net.casualcoding.fxgui.component;

import java.io.IOException;

import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;

public class ScaledBorderPane extends BorderPane
{
	final ScaledBorderPaneController controller;
	
	public ScaledBorderPane()
	{
		controller = load();
	}
	
	private ScaledBorderPaneController load()
	{
       final FXMLLoader loader = new FXMLLoader();
        
        // fx:root is this node.
        loader.setRoot(this);
        
        // The FXMLLoader should use the class loader that loaded
        // this class (ScaledButton).
        loader.setClassLoader(this.getClass().getClassLoader());
        
        // ScaledLabel.fxml contains the configuration for 'this'
        loader.setLocation(this.getClass().getResource("ScaledBorderPane.fxml"));
        
        try
        {
            final Object root = loader.load();
            assert root == this;
        }
        catch (IOException ex)
        {
            throw new IllegalStateException(ex);
        }
        
        final ScaledBorderPaneController scaledBorderPaneController = loader.getController();
        assert scaledBorderPaneController != null;
        return scaledBorderPaneController;
	}	
}
