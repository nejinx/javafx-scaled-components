package net.casualcoding.fxgui.util;

import javafx.scene.text.Text;

public class Measure
{
	// DPI Scaling http://news.kynosarges.org/2013/08/09/javafx-dpi-scaling/
	// root em sizing
	// http://snook.ca/archives/html_and_css/font-size-with-rem
	//final static double em = javafx.scene.text.Font.getDefault().getSize();
	final static double rem = Math.rint(new Text("").getLayoutBounds().getHeight());	// Pixels instead of 'points' as above
	
	public Measure()
	{
		
	}
	
	public static double getEm(double pixels)
	{
		System.out.println("getEm: original: " + pixels + " measured: "  + (pixels / 16.0 * rem));
		return pixels / 16.0 * rem;
	}
	
	public static double getEm()
	{
		return rem;
	}
	
	public static void main(String[] args)
	{
		System.out.println("Size: " + Measure.getEm());
	}	
}
