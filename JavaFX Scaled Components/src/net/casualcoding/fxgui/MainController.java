package net.casualcoding.fxgui;

import net.casualcoding.fxgui.component.ScaledBorderPane;
import net.casualcoding.fxgui.component.ScaledHBox;
import net.casualcoding.fxgui.component.ScaledLabel;
import net.casualcoding.fxgui.component.ScaledTextField;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.text.Font;
import javafx.stage.WindowEvent;

public class MainController
{
	@FXML
	private ScaledLabel scaledLabel;
	@FXML
	private ScaledBorderPane scaledBorderPane;
	@FXML
	private ScaledHBox scaledHBox;
	@FXML
	private ScaledTextField scaledTextField;
	
	//private Main mainApp;
	
	@FXML
	private void initialize()
	{
		System.out.println("Font Size: " + scaledLabel.getFont().getSize());

		scaledLabel.fontProperty().addListener((observable, oldValue, newValue) ->
		{
			System.out.println("font size: " + newValue.getSize());
		});
		
		scaledLabel.setStyle("-fx-border-style: solid; -fx-border-width: 1; -fx-border-color: black; -fx-background-color: white");
		scaledBorderPane.setStyle("-fx-border-style: solid; -fx-border-width: 1; -fx-border-color: black; -fx-background-color: white");
		scaledHBox.setStyle("-fx-border-style: solid; -fx-border-width: 1; -fx-border-color: black; -fx-background-color: white");		
	}
	
	public void setMainApp(Main mainApp)
	{
		//this.mainApp = mainApp;
		
		mainApp.getPrimaryStage().addEventHandler(WindowEvent.WINDOW_SHOWN, event ->
		{
			System.out.println("### Window Shown ###");
		});
	}
}
